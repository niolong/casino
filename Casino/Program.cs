﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casino
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();

            int currentSum;
            int a, b, c;
            int currentRate;
            int currentTry = 1;
            int luckRate = 0, loseRate = 0;
            char answer;

            do
            {
                Console.Clear();

                Console.WriteLine("Добро пожалоать в казино");
                Console.WriteLine("Введите изначальную сумму в банке: ");

                currentSum = int.Parse(Console.ReadLine());

                while (currentSum > 0)
                {
                    Console.WriteLine("Сделайте ставку: ");
                    currentRate = int.Parse(Console.ReadLine());

                    if (currentRate <= 0 || currentSum < currentRate)
                    {
                        while (currentRate > currentSum)
                        {
                            Console.WriteLine("Вы ввели ставку больше имеющийся суммы. Повторите ввод: ");
                            currentRate = int.Parse(Console.ReadLine());
                        }
                    }

                    currentSum = currentSum - currentRate;
                    Console.WriteLine($"Денег осталось в банке: {currentSum}");
                    
                    Console.WriteLine("Нажмите enter чтобы крутить");
                    Console.ReadKey();

                    Console.Write(a = rnd.Next(1, 10));
                    Console.Write(b = rnd.Next(1, 10));
                    Console.WriteLine(c = rnd.Next(1, 10));

                    if (a == 7 && b == 7 && c == 7)
                    {
                        Console.WriteLine("Поздравляем! Вам выпали СЧАСТЛИВЫЕ ТОПАРЫ. Множитель 7x!!");
                        currentSum = currentRate * 7 + currentSum;
                        luckRate++;
                        Console.WriteLine($"Текущая сумма = {currentSum}");
                    }
                    else if (a == 6 && b == 6 && c == 6)
                    {
                        Console.WriteLine("Поздравляем! Вам выпала ДЬЯВОЛЬСКАЯ СТАВКА. Множитель 6x!!");
                        currentSum = currentRate * 6+currentSum;
                        luckRate++;
                        Console.WriteLine($"Текущая сумма = {currentSum}");
                    }
                    else if (a == 2 && b == 2 & c == 8)
                    {
                        Console.WriteLine("Поздравляем! Вам выпала БЛАТНАЯ СТАВКА . Множитель 5x!!");
                        currentSum = currentRate * 5 + currentSum;
                        luckRate++;
                        Console.WriteLine($"Текущая сумма = {currentSum}");
                    }
                    else if (a == b && b == c)
                    {
                        Console.WriteLine("Поздравляем! Вам выпала счастливая ставка. Множитель 4x!!");
                        currentSum = currentRate * 4 + currentSum;
                        luckRate++;
                        Console.WriteLine($"Текущая сумма = {currentSum}");
                    }
                    else if (a == b || b == c || c == a)
                    {
                        Console.WriteLine("Поздравляем! Вам повезло. Множитель 2x!!");
                        currentSum = currentRate * 2 + currentSum;
                        luckRate++;
                        Console.WriteLine($"Текущая сумма = {currentSum}");
                    }
                    else
                    {
                        Console.WriteLine("Вам не повезло. Попробуйте еще раз");
                        loseRate++;
                        Console.WriteLine($"Текущая сумма = {currentSum}");
                    }

                    Console.WriteLine($"Неудачных ставок: {loseRate}");
                    Console.WriteLine($"Удачных ставок: {luckRate}");
                    Console.WriteLine($"Попыток совершено: {currentTry}");

                    currentTry++;
                }

                Console.WriteLine("Хотите повторить игру? (y/n)");
                answer = char.Parse(Console.ReadLine());

            } while (answer=='y');       
        }
    }
}
